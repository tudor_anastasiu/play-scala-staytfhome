package controllers

import java.nio.file
import java.nio.file.Paths

import akka.actor.ActorSystem
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import com.mohiva.play.silhouette.api.{ LoginInfo, Silhouette }
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import forms.InviteEmailForm
import javax.inject.Inject
import models.User
import models.services.UserService
import play.api.i18n.{ I18nSupport, Messages }
import play.api.libs.Files.TemporaryFile
import play.api.libs.json.Json
import play.api.libs.mailer.{ Email, MailerClient }
import play.api.mvc.{ AbstractController, ControllerComponents, MultipartFormData }
import utils.Logger
import utils.auth._

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

class UserManagerController @Inject() (
  userService: UserService,
  silhouette: Silhouette[DefaultEnv],
  cc: ControllerComponents,
  mailerClient: MailerClient,
  actorSystem: ActorSystem,
  assets: Assets,
  assetsFinder: AssetsFinder)(implicit executionContext: ExecutionContext)
  extends AbstractController(cc) with Logger with I18nSupport {
  def list() = silhouette.SecuredAction(WithRole(UserManager) || WithRole(Admin)).async { implicit request =>
    userService.all()
      .map { res =>
        Ok(Json.toJson(res))
      }
  }

  def detail(id: Int) = silhouette.SecuredAction(WithRole(UserManager) || WithRole(Admin)).async { implicit request =>
    userService.findById(id).map {
      case Some(returnedUser) => Ok(Json.toJson(returnedUser))
      case None => NotFound
    }
  }

  def update() = silhouette.SecuredAction(WithRole(UserManager) || WithRole(Admin))(parse.json) { implicit request =>
    val userDataResult = request.body.validate[User]
    userDataResult.fold(
      { errors =>
        logger.error(errors.toString())
        BadRequest("INVALID_FORM")
      },
      { form: User =>
        userService.update(form)
        Ok(Json.toJson(form)).as(JSON)
      }
    )
  }

  def delete(id: Int) = silhouette.SecuredAction(WithRole(UserManager) || WithRole(Admin)).async {
    userService.delete(id).map { _ => Ok }
  }

  def current() = silhouette.SecuredAction(WithRole(SimpleUser) || WithRole(Admin)).async { implicit request =>
    userService.retrieve(request.identity.userId).map { returnedUser =>
      if (returnedUser.isEmpty) {
        BadRequest("USER_NOT_EXISTS")
      }
      Ok(Json.toJson(returnedUser))
    }
  }

  def updateCurrent() = silhouette.SecuredAction(WithRole(SimpleUser)).async(parse.json) { implicit request =>
    val userDataResult = request.body.validate[User]
    userDataResult.fold(
      { errors =>
        logger.error(errors.toString())
        Future.successful(BadRequest("INVALID_FORM"))
      },
      { form: User =>
        userService.retrieve(LoginInfo(CredentialsProvider.ID, form.email)).map {
          case Some(_) =>
            userService.update(form)
            Ok(Json.toJson(form)).as(JSON)
          case None =>
            Unauthorized
        }
      }
    )
  }

  def inviteEmail() = silhouette.SecuredAction(WithRole(Admin)).async { implicit request =>
    InviteEmailForm.form.bindFromRequest.fold(
      form => Future.successful(BadRequest("INVALID_FORM")),
      data => {
        val email = data.email
        userService.retrieve(LoginInfo(CredentialsProvider.ID, email)).map {
          case None =>
            actorSystem.scheduler.scheduleOnce(1 millis) {
              val url = s"http://trips.toptal.com/signup/${request.identity.id.get}"
              mailerClient.send(Email(
                subject = Messages("email.invite.email.subject"),
                from = Messages("email.from"),
                to = Seq(email),
                bodyText = Some(views.txt.emails.inviteEmail(email, url).body),
                bodyHtml = Some(views.html.emails.inviteEmail(email, url).body)
              ))
            }
            Ok("EMAIL_SENT")
          case Some(_) => BadRequest("USER_ALREADY_EXISTS")
        }
      })
  }

  def uploadAvatar() = silhouette.SecuredAction.async(parse.multipartFormData) { implicit request: SecuredRequest[DefaultEnv, MultipartFormData[TemporaryFile]] =>
    request.body.file("avatar").map { picture =>
      val fileName = file.Paths.get(picture.filename).getFileName
      val extension = fileName.toString.split("\\.").last
      val internalAvatarFilename = s"public/uploads/${request.identity.userId}.$extension"
      picture.ref.moveTo(Paths.get(internalAvatarFilename), replace = true)
      //      assets.at(avatarFilename).apply().
      //      assetsFinder.path(avatarFilename)
      //      controllers.Assets.Asset.apply(avatarFilename).name
      val avatarFilename = s"assets/uploads/${request.identity.userId}.$extension"
      userService.save(request.identity.copy(avatarURL = Some(s"http://10.0.2.2:9000/$avatarFilename"))).map { _ =>
        Ok("File uploaded")
      }
    }.getOrElse {
      Future.successful(BadRequest)
    }
  }
}
