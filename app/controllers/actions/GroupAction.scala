package controllers.actions

import com.google.inject.Inject
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import models.Group
import models.services.GroupManagerService
import play.api.Logger
import play.api.mvc._
import utils.auth.DefaultEnv

import scala.concurrent.{ ExecutionContext, Future }

case class GroupRequest[Env <: DefaultEnv, B](group: Group, securedRequest: SecuredRequest[Env, B])
  extends WrappedRequest[B](securedRequest.request)

class GroupAction @Inject() (
  val groupManagerService: GroupManagerService,
  val parser: BodyParsers.Default)(implicit val executionContext: ExecutionContext)
  extends ActionRefiner[({ type R[B] = SecuredRequest[DefaultEnv, B] })#R, ({ type R[B] = GroupRequest[DefaultEnv, B] })#R] {
  var groupId: Option[Int] = None
  val logger: Logger = Logger(this.getClass())

  def setId(id: Int): GroupAction = {
    groupId = Some(id)
    this
  }

  override protected def refine[A](securedRequest: SecuredRequest[DefaultEnv, A]): Future[Either[Result, GroupRequest[DefaultEnv, A]]] = {
    require(groupId.isDefined)
    groupManagerService.findById(groupId.get)(securedRequest.identity).map {
      case None => Left(Results.Forbidden)
      case Some(group) => Right(new GroupRequest(group, securedRequest))
    }
  }
}
