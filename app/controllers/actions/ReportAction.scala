package controllers.actions

import com.google.inject.Inject
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import models.Report
import models.services.ReportManagerService
import play.api.Logger
import play.api.mvc._
import utils.auth.DefaultEnv

import scala.concurrent.{ ExecutionContext, Future }

case class ReportRequest[Env <: DefaultEnv, B](report: Report, securedRequest: SecuredRequest[Env, B])
  extends WrappedRequest[B](securedRequest.request)

class ReportAction @Inject() (
  val reportManagerService: ReportManagerService,
  val parser: BodyParsers.Default)(implicit val executionContext: ExecutionContext)
  extends ActionRefiner[({ type R[B] = SecuredRequest[DefaultEnv, B] })#R, ({ type R[B] = ReportRequest[DefaultEnv, B] })#R] {
  var reportId: Option[Int] = None
  val logger: Logger = Logger(this.getClass())

  def setId(id: Int): ReportAction = {
    reportId = Some(id)
    this
  }

  override protected def refine[A](securedRequest: SecuredRequest[DefaultEnv, A]): Future[Either[Result, ReportRequest[DefaultEnv, A]]] = {
    require(reportId.isDefined)
    reportManagerService.findById(reportId.get)(securedRequest.identity).map {
      case None => Left(Results.Forbidden)
      case Some(report) => Right(new ReportRequest(report, securedRequest))
    }
  }
}
