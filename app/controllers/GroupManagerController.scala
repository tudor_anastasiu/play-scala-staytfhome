package controllers

import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import controllers.actions.{ GroupAction, GroupRequest }
import forms.GroupForm
import javax.inject.Inject
import models.Group
import models.services.GroupManagerService
import play.api.libs.json.Json
import play.api.mvc.{ AbstractController, AnyContent, ControllerComponents }
import utils.Logger
import utils.auth.{ DefaultEnv, SimpleUser, WithRole }

import scala.concurrent.{ ExecutionContext, Future }

/**
 * The `Trip Manager` controller. Allows operations owned only by the currently authenticated user.
 *
 * @param cc                     The Play controller components.
 * @param groupManagerService    The group manager service which is connected to the backing store.
 * @param silhouette             The Silhouette stack.
 * @param groupAction            The action that forbids operations on records not owned by the logged in user.
 * @param executionContext       The execution context.
 */
class GroupManagerController @Inject() (
  cc: ControllerComponents,
  groupManagerService: GroupManagerService,
  silhouette: Silhouette[DefaultEnv],
  groupAction: GroupAction)(implicit executionContext: ExecutionContext)
  extends AbstractController(cc) with Logger {

  def list() = silhouette.SecuredAction(WithRole(SimpleUser)).async { implicit request: SecuredRequest[DefaultEnv, AnyContent] =>
    groupManagerService.all()(request.identity)
      .map { res =>
        Ok(Json.toJson(res.map(GroupForm(_))))
      }
  }

  def detail(id: Int) = (
    silhouette.SecuredAction(WithRole(SimpleUser)) andThen
    groupAction.setId(id)).async { implicit request: GroupRequest[DefaultEnv, AnyContent] =>
      groupManagerService.findById(id)(request.securedRequest.identity).map { returnedGroup =>
        Ok(Json.toJson(returnedGroup.map { group => GroupForm(group) }))
      }
    }

  def create() = silhouette.SecuredAction(WithRole(SimpleUser))(parse.json).async { implicit request =>
    val tripDataResult = request.body.validate[GroupForm]
    tripDataResult.fold(
      { errors =>
        logger.error(errors.toString())
        Future.successful(BadRequest("INVALID_FORM"))
      },
      { form: GroupForm =>
        groupManagerService.insert(Group(None, form))(request.identity)
          .map(GroupForm(_))
          .map(mealData => Ok(Json.toJson(mealData)).as(JSON))
      })
  }

  def update() = (silhouette.SecuredAction(WithRole(SimpleUser)))(parse.json) { implicit request =>
    val tripDataResult = request.body.validate[GroupForm]
    tripDataResult.fold(
      { errors =>
        logger.error(errors.toString())
        BadRequest("INVALID_FORM")
      },
      { form: GroupForm =>
        groupManagerService.update(Group(form.id, form))(request.identity)
        Ok(Json.toJson(form)).as(JSON)
      }
    )
  }

  def delete(id: Int) = (silhouette.SecuredAction(WithRole(SimpleUser)) andThen
    groupAction.setId(id)).async { implicit request =>
      groupManagerService.delete(id)(request.securedRequest.identity).map { _ => Ok }
    }
}
