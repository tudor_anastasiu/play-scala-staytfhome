package controllers

import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import controllers.actions.{ ReportAction, ReportRequest }
import forms.ReportForm
import javax.inject.Inject
import models.Report
import models.services.ReportManagerService
import play.api.libs.json.Json
import play.api.mvc.{ AbstractController, AnyContent, ControllerComponents }
import utils.Logger
import utils.auth.{ DefaultEnv, SimpleUser, WithRole }

import scala.concurrent.{ ExecutionContext, Future }

/**
 * The `Report Manager` controller. Allows operations owned only by the currently authenticated user.
 *
 * @param cc                     The Play controller components.
 * @param reportManagerService   The report manager service which is connected to the backing store.
 * @param silhouette             The Silhouette stack.
 * @param reportAction           The action that forbids operations on records not owned by the logged in user.
 * @param executionContext       The execution context.
 */
class ReportManagerController @Inject() (
  cc: ControllerComponents,
  reportManagerService: ReportManagerService,
  silhouette: Silhouette[DefaultEnv],
  reportAction: ReportAction)(implicit executionContext: ExecutionContext)
  extends AbstractController(cc) with Logger {

  def list() = silhouette.SecuredAction(WithRole(SimpleUser)).async { implicit request: SecuredRequest[DefaultEnv, AnyContent] =>
    reportManagerService.all()(request.identity)
      .map { res =>
        Ok(Json.toJson(res.map(ReportForm(_))))
      }
  }

  def detail(id: Int) = (
    silhouette.SecuredAction(WithRole(SimpleUser)) andThen
    reportAction.setId(id)).async { implicit request: ReportRequest[DefaultEnv, AnyContent] =>
      reportManagerService.findById(id)(request.securedRequest.identity).map { returnedReport =>
        Ok(Json.toJson(returnedReport.map { report => ReportForm(report) }))
      }
    }

  def create() = silhouette.SecuredAction(WithRole(SimpleUser))(parse.json).async { implicit request =>
    val tripDataResult = request.body.validate[ReportForm]
    tripDataResult.fold(
      { errors =>
        logger.error(errors.toString())
        Future.successful(BadRequest("INVALID_FORM"))
      },
      { form: ReportForm =>
        reportManagerService.insert(Report(None, form))(request.identity)
          .map(ReportForm(_))
          .map(mealData => Ok(Json.toJson(mealData)).as(JSON))
      })
  }

  def update() = (silhouette.SecuredAction(WithRole(SimpleUser)))(parse.json) { implicit request =>
    val tripDataResult = request.body.validate[ReportForm]
    tripDataResult.fold(
      { errors =>
        logger.error(errors.toString())
        BadRequest("INVALID_FORM")
      },
      { form: ReportForm =>
        reportManagerService.update(Report(form.id, form))(request.identity)
        Ok(Json.toJson(form)).as(JSON)
      }
    )
  }

  def delete(id: Int) = (silhouette.SecuredAction(WithRole(SimpleUser)) andThen
    reportAction.setId(id)).async { implicit request =>
      reportManagerService.delete(id)(request.securedRequest.identity).map { _ => Ok }
    }
}
