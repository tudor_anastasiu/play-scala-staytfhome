package modules

import com.google.inject.AbstractModule
import models.daos._
import models.services._
import net.codingwell.scalaguice.ScalaModule

/**
 * The base Guice module.
 */
class BaseModule extends AbstractModule with ScalaModule {

  /**
   * Configures the module.
   */
  override def configure(): Unit = {
    bind[GroupDAO].to[GroupDAOImpl]
    bind[GroupManagerService].to[GroupManagerServiceImpl]

    bind[ReportDAO].to[ReportDAOImpl]
    bind[ReportManagerService].to[ReportManagerServiceImpl]
  }
}
