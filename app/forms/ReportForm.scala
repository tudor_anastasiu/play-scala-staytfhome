package forms

import java.time.ZonedDateTime

import models.Report
import play.api.libs.json.{ Json, OFormat }

case class ReportForm(
  id: Option[Int],
  symptoms: Seq[String],
  latitude: Double,
  longitude: Double,
  comment: String,
  testStatus: String,
  dateTime: ZonedDateTime,
  userId: Option[Int])

object ReportForm {
  implicit val jsonFormat: OFormat[ReportForm] = Json.format[ReportForm]

  def apply(report: Report) = Some(Report(
    report.id,
    report.symptoms,
    report.latitude,
    report.longitude,
    report.comment,
    report.testStatus,
    report.dateTime,
    report.userId
  ))
}
