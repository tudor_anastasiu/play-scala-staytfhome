package forms

import models.Group
import play.api.libs.json.{ Json, OFormat }

case class GroupForm(
  id: Option[Int],
  title: String,
  address: String,
  geolocation: Option[String],
  comment: String,
  userId: Option[Int])

object GroupForm {
  implicit val jsonFormat: OFormat[GroupForm] = Json.format[GroupForm]

  def apply(group: Group) = Some(Group(
    group.id,
    group.title,
    group.address,
    group.geolocation,
    group.comment,
    group.userId
  ))
}
