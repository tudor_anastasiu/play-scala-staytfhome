package models

import forms.GroupForm
import play.api.libs.json.{ Json, OFormat }

case class Group(
  id: Option[Int],
  title: String,
  address: String,
  geolocation: Option[String],
  comment: String,
  userId: Option[Int])

object Group {
  implicit val jsonFormat: OFormat[Group] = Json.format[Group]

  def apply(id: Option[Int], form: GroupForm): Group = Group(
    id,
    form.title,
    form.address,
    form.geolocation,
    form.comment,
    form.userId
  )

  def mapperTo(
    id: Option[Int],
    title: String,
    address: String,
    geolocation: Option[String],
    comment: String,
    userId: Option[Int]) = apply(
    id,
    title,
    address,
    geolocation,
    comment,
    userId)

  def mapperFrom(group: Group) = Some((
    group.id,
    group.title,
    group.address,
    group.geolocation,
    group.comment,
    group.userId))
}
