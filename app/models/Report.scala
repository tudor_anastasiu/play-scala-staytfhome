package models

import java.time.ZonedDateTime

import forms.ReportForm
import play.api.libs.json.{ Json, OFormat }

case class Report(
  id: Option[Int],
  symptoms: Seq[String],
  latitude: Double,
  longitude: Double,
  comment: String,
  testStatus: String,
  dateTime: ZonedDateTime,
  userId: Option[Int]
)

object Report {
  implicit val jsonFormat: OFormat[Report] = Json.format[Report]

  def apply(id: Option[Int], form: ReportForm): Report = Report(
    id,
    form.symptoms,
    form.latitude,
    form.longitude,
    form.comment,
    form.testStatus,
    form.dateTime,
    form.userId
  )

  def mapperTo(
    id: Option[Int],
    symptoms: String,
    latitude: Double,
    longitude: Double,
    comment: String,
    testStatus: String,
    dateTime: ZonedDateTime,
    userId: Option[Int]) = apply(
    id,
    symptoms.split(","),
    latitude,
    longitude,
    comment,
    testStatus,
    dateTime,
    userId)

  def mapperFrom(report: Report) = Some((
    report.id,
    report.symptoms.mkString(","),
    report.latitude,
    report.longitude,
    report.comment,
    report.testStatus,
    report.dateTime,
    report.userId))

}
