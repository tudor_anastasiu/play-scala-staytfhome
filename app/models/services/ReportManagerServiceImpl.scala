package models.services

import com.google.inject.Inject
import models.daos.ReportDAO
import models.{ Report, User }

import scala.concurrent.Future

class ReportManagerServiceImpl @Inject() (reportDAO: ReportDAO)
  extends ReportManagerService {
  def all()(user: User): Future[Seq[Report]] = reportDAO.all()(user)

  def insert(report: Report)(user: User): Future[Report] = reportDAO.insert(report)(user)

  def findById(id: Int)(user: User): Future[Option[Report]] = reportDAO.findById(id)(user)

  def update(report: Report)(user: User): Future[Unit] = reportDAO.update(report)(user)

  def delete(id: Int)(user: User): Future[Unit] = reportDAO.delete(id)(user)
}
