package models.services

import com.google.inject.Inject
import models.{ Group, User }
import models.daos.GroupDAO

import scala.concurrent.Future

class GroupManagerServiceImpl @Inject() (groupDAO: GroupDAO)
  extends GroupManagerService {
  def all()(user: User): Future[Seq[Group]] = groupDAO.all()(user)

  def insert(group: Group)(user: User): Future[Group] = groupDAO.insert(group)(user)

  def findById(id: Int)(user: User): Future[Option[Group]] = groupDAO.findById(id)(user)

  def update(group: Group)(user: User): Future[Unit] = groupDAO.update(group)(user)

  def delete(id: Int)(user: User): Future[Unit] = groupDAO.delete(id)(user)
}
