package models.daos

import models.{ Group, User }

import scala.concurrent.Future

trait GroupDAO {
  def all()(user: User): Future[Seq[Group]]

  def insert(group: Group)(user: User): Future[Group]

  def findById(id: Int)(user: User): Future[Option[Group]]

  def update(group: Group)(user: User): Future[Unit]

  def delete(id: Int)(user: User): Future[Unit]
}
