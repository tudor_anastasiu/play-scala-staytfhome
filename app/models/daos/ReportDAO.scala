package models.daos

import models.{ Report, User }

import scala.concurrent.Future

trait ReportDAO {
  def all()(user: User): Future[Seq[Report]]

  def insert(report: Report)(user: User): Future[Report]

  def findById(id: Int)(user: User): Future[Option[Report]]

  def update(report: Report)(user: User): Future[Unit]

  def delete(id: Int)(user: User): Future[Unit]
}
