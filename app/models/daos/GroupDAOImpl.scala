package models.daos

import javax.inject.Inject
import models.{ Group, User }
import play.api.Logger
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import slick.jdbc.MySQLProfile

import scala.concurrent.{ ExecutionContext, Future }

class GroupDAOImpl @Inject() (
  protected val userDAO: UserDAOImpl,
  protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext)
  extends HasDatabaseConfigProvider[MySQLProfile] with GroupDAO {
  import profile.api._

  val Groups = TableQuery[GroupTable]

  /**
   * Group Manager Operations
   */

  def all()(user: User): Future[Seq[Group]] = db.run(Groups.findBy(_.userId).applied(user.id.get).result)

  val logger: Logger = Logger(this.getClass())

  def insert(group: Group)(user: User): Future[Group] = {
    val newGroup = group.copy(userId = user.id)
    db.run((Groups returning Groups.map(_.id) into ((item, id) => item.copy(id = Some(id)))) += newGroup)
  }

  def findById(id: Int)(user: User): Future[Option[Group]] = {
    db.run(Groups.filter(_.userId === user.id).filter(_.id === id).result.headOption)
  }

  def update(group: Group)(user: User): Future[Unit] = {
    require(group.id.isDefined)
    val _meal = group.copy(userId = user.id)

    db.run(Groups.insertOrUpdate(_meal)).map(_ => ())
  }

  def delete(id: Int)(user: User): Future[Unit] = {
    db.run(Groups.findBy(_.id).applied(id).filter(_.userId === user.id).delete).map(_ => ())
  }

  protected class GroupTable(tag: Tag) extends Table[Group](tag, "GROUP") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def title = column[String]("TITLE")
    def address = column[String]("ADDRESS")
    def geolocation = column[String]("GEOLOCATION")
    def comment = column[String]("COMMENT")
    def userId = column[Int]("USER_ID")

    def user = foreignKey("FK_USERGROUPS", userId, userDAO.Users)(_.id, onDelete = ForeignKeyAction.Cascade)

    def * = (id.?, title, address, geolocation.?, comment, userId.?) <> ((Group.mapperTo _).tupled, Group.mapperFrom)
  }

}
