package models.daos

import java.time.ZonedDateTime

import javax.inject.Inject
import models.{ Report, User }
import play.api.Logger
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import slick.jdbc.MySQLProfile

import scala.concurrent.{ ExecutionContext, Future }

class ReportDAOImpl @Inject() (
  protected val userDAO: UserDAOImpl,
  protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext)
  extends HasDatabaseConfigProvider[MySQLProfile] with ReportDAO {
  import profile.api._

  val Reports = TableQuery[ReportTable]

  /**
   * Report Manager Operations
   */

  def all()(user: User): Future[Seq[Report]] = db.run(Reports.result)

  val logger: Logger = Logger(this.getClass())

  def insert(report: Report)(user: User): Future[Report] = {
    val newGroup = report.copy(userId = user.id)
    db.run((Reports returning Reports.map(_.id) into ((item, id) => item.copy(id = Some(id)))) += newGroup)
  }

  def findById(id: Int)(user: User): Future[Option[Report]] = {
    db.run(Reports.filter(_.userId === user.id).filter(_.id === id).result.headOption)
  }

  def update(report: Report)(user: User): Future[Unit] = {
    require(report.id.isDefined)
    val _meal = report.copy(userId = user.id)

    db.run(Reports.insertOrUpdate(_meal)).map(_ => ())
  }

  def delete(id: Int)(user: User): Future[Unit] = {
    db.run(Reports.findBy(_.id).applied(id).filter(_.userId === user.id).delete).map(_ => ())
  }

  protected class ReportTable(tag: Tag) extends Table[Report](tag, "REPORT") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def symptoms = column[String]("SYMPTOMS")
    def latitude = column[Double]("LATITUDE")
    def longitude = column[Double]("LONGITUDE")
    def comment = column[String]("COMMENT")
    def testStatus = column[String]("TEST_STATUS")
    def dateTime = column[ZonedDateTime]("DATE_TIME")
    def userId = column[Int]("USER_ID")

    def user = foreignKey("FK_USERREPORTS", userId, userDAO.Users)(_.id, onDelete = ForeignKeyAction.Cascade)

    def * = (id.?, symptoms, latitude, longitude, comment, testStatus, dateTime, userId.?) <> ((Report.mapperTo _).tupled, Report.mapperFrom)
  }

}
