-- Report schema

-- !Ups

CREATE TABLE Report (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    symptoms varchar(50) NOT NULL,
    latitude float NOT NULL,
    longitude float NOT NULL,
    comment varchar(1000) NOT NULL,
    test_status varchar(10) NOT NULL DEFAULT '-1',
    user_id int NOT NULL,
    date_time text NOT NULL
) ENGINE=InnoDB;

ALTER TABLE Report
ADD CONSTRAINT FK_UserReports
FOREIGN KEY (user_id) REFERENCES User(id)
ON DELETE CASCADE;

-- !Downs

ALTER TABLE Report
DROP FOREIGN KEY FK_UserReports;

DROP TABLE Report;
