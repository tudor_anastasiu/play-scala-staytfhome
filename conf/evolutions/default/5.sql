-- Group schema

-- !Ups

CREATE TABLE `Group` (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title varchar(50) NOT NULL,
    address varchar(50) NOT NULL,
    geolocation text,
    comment varchar(1000) NOT NULL,
    user_id int NOT NULL
) ENGINE=InnoDB;

ALTER TABLE `Group`
ADD CONSTRAINT FK_UserGroups
FOREIGN KEY (user_id) REFERENCES User(id)
ON DELETE CASCADE;

-- !Downs

ALTER TABLE `Group`
DROP FOREIGN KEY FK_UserGroups;

DROP TABLE `Group`;
